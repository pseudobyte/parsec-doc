# Co-Op Play in Cuphead

## One Keyboard and one Controller

If one keyboard and one controller are present, cuphead will only allow them to be different players if the keyboard is player 1. To do this, press a key on the keyboard on the start screen:

![cuphead start screen](cuphead_start.jpg)

If you started the game with the controller, you can also remove it from the settings:

![cuphead input settings](cuphead_settings.png)

Once the player on keyboard has started the game, the controller player can join in by pressing any button. 

## Two Keyboards

Cuphead does not have native support for two players on keyboard, so you will need to use at least one controller or emulate one using [this method](https://support.parsecgaming.com/hc/en-us/articles/360012652092-Playing-Games-With-Two-Keyboards-Emulating-A-Controller-With-A-Keyboard).