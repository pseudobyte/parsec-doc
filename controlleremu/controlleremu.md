# Emulating a Controller with a Keyboard

## Introduction

This is recommended for clients that do not have controllers and don't want to inconvenience hosts with allowing keyboard controls, or who want to play games that will not allow multiple players on keyboard.

## Installation

On the client computer, get [UCR](https://github.com/Snoothy/UCR/releases/latest) ([UCR readme](https://github.com/Snoothy/UCR#universal-control-remapper)) and unzip it, and install [Interception](https://github.com/snoothy/ucr/wiki/Core_Interception) to allow for the use of keyboard inputs. UCR requires [ViGEm](https://github.com/nefarius/ViGEm) to emulate Xbox 360 controllers, but you will already have it if you installed Parsec with controller support. Be sure you reboot after installing either Interception or ViGEm.

## Creating a profile

After rebooting, start UCR. If you extracted UCR with Windows's zip extractor, you may be prompted to run it by Microsoft SmartScreen. You will then be prompted by UCR to unblock additional components of UCR, which will require going through SmartScreen again. You may need to restart UCR before any available devices show up in the following step. Using an alternate extractor like [7-zip](https://www.7-zip.org/) will avoid this.

Select `Devices > Manage device groups` in the menu. Create an input group and add your keyboard to it from the Core_Interception category.

![demonstration of input group setup](input_group.png)

Create an output group and add the ViGEm Xbox 360 Controller 1 to it.

![demonstration of output group setup](output_group.png)

Close the device groups window and select `Profile > New` in the menu. Give it a name and select your input and output groups.

![demonstration of profile creation](profile.png)

Double click your new profile to edit it.

## Mapping Inputs

### Digital Inputs

To map a digital button, which includes the DPad, but does not include the triggers (L2 and R2), type a mapping name in the mapping field at the top and click add. then select `Button to Button` in the plugin dropdown and click add. Click `Click to Bind` on the left side to press a key to use, or click the arrow next to it to select a key from a menu. On the right, select the button to use from a dropdown.

![example of mapping a controller button](button_mapping.png)

### Single Axis (Triggers)

To map the triggers, use the `Button to Axis` plugin. Map the buttons the same way as with digital inputs, but check the `Absolute` checkbox. This will ensure that the trigger goes between 0 and 100, not 50 and 100.

![example of mapping a controller trigger](trigger_mapping.png)

### Double Axis (Joysticks)

To map an axis, use the `Buttons to Axis` plugin. You will need two of these for each joystick, e.g. LX and LY for the left joystick. Bind these the same as digital inputs, except there are two buttons on the keyboard, one for the low part of the axis and one for the high part. Note that as of UCR v0.6.0, low and high seem to be reversed, so be sure to test your bindings to make sure they do what you expect. If you find that your two buttons are the wrong way around, you can check the `Invert` checkbox to swap them.

![example of mapping a controller axis](axis_mapping.png)

To test your mappings, you can select `Profile > Activate Profile` in the menu and visit the [HTML5 Gamepad Tester](http://html5gamepad.com/). You can also see the status of inputs and outputs using the green bars below mappings in UCR. You may need to deactivate and activate the profile to apply new changes. When you are done, close the profile window and select `File > Save` in UCR's main menu. Changes are _not_ automatically saved.

## Using the Virtual Controller

You will need to run UCR and activate your profile when you wish to have your virtual controller active.

## Troubleshooting

If the ViGEm controllers do not appear as options for output devices, you do not have ViGEm installed. You can install it from [here](https://support.parsecgaming.com/hc/en-us/articles/360015190691-Controller-Drivers).

If your virtual controller is not recognized by the host, they are most likely missing the [controller driver](https://support.parsecgaming.com/hc/en-us/articles/360015190691-Controller-Drivers).

You will still be able to use your keyboard normally (unless you block inputs as below), but if you notice extra key presses being added or the mouse being triggered, this is probably caused by Steam controller support. Follow [these instructions](https://support.parsecgaming.com/hc/en-us/articles/360011883691) to disable it. This can happen on either host or client if steam is open.

By default, keyboard inputs are still passed through. It's recommended the host does not give you keyboard access to avoid you pressing whatever buttons you mapped when you use your controller. It is possible to [block keyboard inputs with UCR/Interception](https://github.com/snoothy/ucr/wiki/Core_Interception#blocking-with-interception), but **read the instructions and warnings very carefully**, because it is possible to create bindings that make it difficult to operate your computer until you deactivate the profile/exit UCR/reboot your computer.
